package com.example.parth.customcamera;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

/**
 * Created by parth on 5/2/19.
 */

public class ShowCamera extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "ShowCamera";
    Camera mCamera;
    Context context;

    SurfaceHolder holder;

    public ShowCamera(Context context, Camera camera) {
        super(context);
        this.context = context;
        this.mCamera = camera;
        holder = getHolder();

        holder.addCallback(this);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
       /* try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting mCamera preview: " + e.getMessage());
        }*/

        Camera.Parameters parameters = mCamera.getParameters();

        //get mCamera pixel sizes
        List<Camera.Size> sizes = parameters.getSupportedPictureSizes();
        Camera.Size mSize = null;

        for (Camera.Size size : sizes) {
            mSize = size;
        }


        //Change orientation

        if (this.getResources().getConfiguration().orientation != Configuration
                .ORIENTATION_LANDSCAPE) {
            parameters.set("orientation", "portrait");
            mCamera.setDisplayOrientation(90);
            parameters.setRotation(90);
        } else {
            parameters.set("orientation", "landscape");
            mCamera.setDisplayOrientation(0);
            parameters.setRotation(0);
        }
        parameters.setPictureSize(mSize.width, mSize.height);

        mCamera.setParameters(parameters);
        try {
            mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCamera.startPreview();
    }

    @Override
    public void surfaceChanged(SurfaceHolder mHolder, int format, int width, int height) {
        if (mHolder.getSurface() == null) {
            // preview surface does not exist
            return;
        }

        // stop preview before making changes
        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore: tried to stopjava.lang.RuntimeException: Camera is being used after Camera.release() was called a non-existent preview
        }

        // set preview size and make any resize, rotate or
        // reformatting changes here

        // start preview with new settings
        try {
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            Log.d(TAG, "Error starting camera preview: " + e.getMessage());
        }

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder mHolder) {
// If your preview can change or rotate, take care of those events here.
        // Make sure to stop the preview before resizing or reformatting it.
        Log.d("MainActivity","onSurface Destroyed");
        mCamera.stopPreview();
        mCamera.release();

    }
}

