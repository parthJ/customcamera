package com.example.parth.customcamera;

import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    Camera camera;
    FrameLayout mFrameLayout;

    ShowCamera showCamera;

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("MainActivity","onResume");
       // openCamera();
        //showCamera = new ShowCamera(this,camera);
        //camera=Camera.open();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("MainActivity","onStop");

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("MainActivity","onPause");

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MainActivity","onCreate");

        setContentView(R.layout.activity_main);

        mFrameLayout = findViewById(R.id.frameLayout);
        camera=getCameraInstance();
      //  camera=Camera.open();
        showCamera = new ShowCamera(this,camera);
        mFrameLayout.addView(showCamera);
    }

    Camera.PictureCallback mPictureCallback=new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            File picture_file=getOutputMediaFile();
            if(picture_file==null){
                return;
            }else{
                try {
                    FileOutputStream fos=new FileOutputStream(picture_file);
                    fos.write(data);
                    fos.close();
                    camera.startPreview();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        private File getOutputMediaFile() {
            String state= Environment.getExternalStorageState();
                    if(!Objects.equals(state, Environment.MEDIA_MOUNTED)){
                return  null;
                    }else{
                        File folder_gui=new File(Environment.getExternalStorageDirectory()+File
                                .separator+"GUI");
                        if(!folder_gui.exists()){
                            folder_gui.mkdirs();
                        }
                        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                        File outputFile=new File(folder_gui,"IMG_"+timeStamp+".jpeg");
                        return outputFile;
                    }
        }
    };

    public void captureImage(View v) {
     if(camera!=null){
         camera.takePicture(null,null,mPictureCallback);
     }
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

}
